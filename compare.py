#! /usr/bin/python3
"""This program fetches the directory string and
hash for any file in the system"""

import hashlib
import os
import sys
import json 
from thefuzz import fuzz


def create_pair(dirs, hash_sha):
    """Create a readable dictionary of directory and hash"""
    pair = {}
    i = 0
    while i != len(dirs):
        pair[dirs[i]] = hash_sha[i]
        i += 1
    return pair

def create_list(it1):
    """This function searches for specific file
    starting from root directory down"""
    # Create a list
    filelist = []
    # For loop through directories
    for root, dirs, files in os.walk("/"):
        for file in files:
            if file.lower() == it1.lower():
                file_path = f"{root}/{file}"
                filelist.append(file_path)
    # return the list
    return filelist

def hash_file(filename):
    """This function returns the SHA-1 hash
    of the file passed into it"""
    # make a hash object
    new_hash = hashlib.sha256()
    # open file for reading in binary mode
    with open(filename,'rb') as file:

       # loop till the end of the file
        chunk = 0
        while chunk != b'':
           # read only 1024 bytes at a time
            chunk = file.read(1024)
            new_hash.update(chunk)

    # return the hex representation of digest
    return new_hash.hexdigest()

def main():
    """This function prompts for the file to search for
    creates a list and iterates the list
    then prints a  hash
    """
    if len(sys.argv) > 1:
        # Prompt user for file name
        search_item = sys.argv[1]
    else:
        search_item = input("What file would you like to search for: " + "\n")

    # Create list
    output = []
    new_list = create_list(search_item)
    for item in new_list:
        output.append(hash_file(item))
    pair_dict = create_pair(new_list, output)
    res = json.dumps(pair_dict, sort_keys=True, indent=4)
    # Print out the hashes
    print(res)

main()
